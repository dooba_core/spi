/* Dooba SDK
 * SPI Communication
 */

// External Includes
#include <avr/io.h>
#include <util/delay.h>
#include <dio/dio.h>

// Internal Includes
#include "spi.h"

// Initialize SPI Interface
void spi_init()
{
	// Pre-Init
	spi_preinit();

	// Enable SPI as Master - Max Speed (fOSC / 2) - No Interrupts
	SPCR = _BV(SPE) | _BV(MSTR);
	SPSR = _BV(SPI2X);
}

// Pre-Initialize SPI Interface
void spi_preinit()
{
	// Configure MOSI & SCK as Output Pins, MISO as Input Pin with Pull-up
	dio_output(SPI_PIN_MOSI);
	dio_input(SPI_PIN_MISO);
	dio_output(SPI_PIN_SCK);
	dio_lo(SPI_PIN_MOSI);
	dio_hi(SPI_PIN_MISO);
	dio_lo(SPI_PIN_SCK);
}

// Switch Configuration
uint8_t spi_switch_config(uint8_t cfg)
{
	uint8_t result;
	result = SPCR;
	SPCR = cfg;
	return result;
}

// Perform I/O
uint8_t spi_io(uint8_t d)
{
	// Transmit a byte
	SPDR = d;

	// Wait for transmit
	while(!(SPSR & _BV(SPIF)))				{ /* NoOp */ }

	// Read byte
	d = SPDR;
	return d;
}

// Fast Transmit
void spi_tx(void *d, uint16_t s)
{
	uint16_t i;
	for(i = 0; i < s; i = i + 1)			{ SPDR = ((uint8_t *)d)[i]; while(!(SPSR & _BV(SPIF))) { /* NoOp */ } }
}

// Fast Transmit - Single Byte
void spi_tx_s(uint8_t d, uint16_t s)
{
	uint16_t i;
	for(i = 0; i < s; i = i + 1)			{ SPDR = d; while(!(SPSR & _BV(SPIF))) { /* NoOp */ } }
}

// Fast Receive
void spi_rx(void *d, uint16_t s)
{
	uint16_t i;
	for(i = 0; i < s; i = i + 1)			{ SPDR = 0xff; while(!(SPSR & _BV(SPIF))) { /* NoOp */ } ((uint8_t *)d)[i] = SPDR; }
}
