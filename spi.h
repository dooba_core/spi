/* Dooba SDK
 * SPI Communication
 */

#ifndef	__SPI_H
#define	__SPI_H

// External Includes
#include <stdint.h>
#include <avr/io.h>

// Define Defaults
#ifndef	SPI_PIN_MOSI
#define	SPI_PIN_MOSI				13
#endif
#ifndef	SPI_PIN_MISO
#define	SPI_PIN_MISO				14
#endif
#ifndef	SPI_PIN_SCK
#define	SPI_PIN_SCK					15
#endif

// Initialize SPI Interface
extern void spi_init();

// Pre-Initialize SPI Interface
extern void spi_preinit();

// Switch Configuration
extern uint8_t spi_switch_config(uint8_t cfg);

// Perform I/O
extern uint8_t spi_io(uint8_t d);

// Fast Transmit
extern void spi_tx(void *d, uint16_t s);

// Fast Transmit - Single Byte
extern void spi_tx_s(uint8_t d, uint16_t s);

// Fast Receive
extern void spi_rx(void *d, uint16_t s);

#endif
